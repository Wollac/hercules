package db

import "errors"

var ErrTransactionTooBig = errors.New("transaction too big")
